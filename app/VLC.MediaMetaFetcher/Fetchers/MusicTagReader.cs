﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VLC.Database;
using Windows.Storage.FileProperties;
using Windows.Storage;
using System.IO;

namespace VLC.MusicMetaFetcher.Fetchers
{
    public static class MusicTagReader
    {

        public static async Task<byte[]> ReadAlbumCoverFromTag(string filePath)
        {
            StorageFile musicFile = await StorageFile.GetFileFromPathAsync(filePath);
            StorageItemThumbnail img =  await musicFile.GetThumbnailAsync(ThumbnailMode.MusicView,600);    
            if(img != null && img.Type == ThumbnailType.Image)
            {
                byte[] imgBytes = new byte[img.Size];
                Stream imgStream = img.AsStream();
                imgStream.Position = 0;
                await imgStream.ReadAsync(imgBytes, 0, Convert.ToInt32(img.Size));
                return imgBytes;
            }
            return null;
        }
    }
}
